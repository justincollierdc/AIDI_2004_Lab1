# AIDI-2004 Lab 1 : Placeholder Code File
# Name: Justin Collier | 100345263
# Date: 5/24/2024
# Purpose: This file is a placeholder that will be used to demonstrate GIT basics and repository use.

list_of_examples = []

# Account Class
class Example:
    # Variable Declaration
    name = ""
    message = "" 

    # Parameterized Constructor
    def __init__(self, name, message):
        self.name = name
        self.message = message

    # Display Example Info
    def display_example(self):
        print(str(self.name) + " - " + str(self.message))


# [Account] Class Test Cases
ex_01 = Example("Jason Smith", "Hello World")
list_of_examples.append(ex_01)
ex_02 = Example("John Doe", "Goodbye")
list_of_examples.append(ex_02)

print("Current examples in our system...")
for x in list_of_examples:
  x.display_example()